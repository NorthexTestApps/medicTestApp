//
//  IRConnectionManager.h
//  Informator
//
//  Created by Александр Сенченков on 11.11.16.
//  Copyright © 2016 Александр Сенченков. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>
#import "Constants.h"

static const NSString *APIToken = @"tPK7s7vdmDxZf7Ar";

@interface NXConnectionManager : AFHTTPSessionManager

- (NSError *) noConnectionError;

+ (NXConnectionManager *)instance;

// Get all news
- (void)getAllNewsWithParam:(NSDictionary *)params callback:(Callback) callback;

//Get info about new
- (void)getInfoAboutNewWithParam:(NSDictionary *)params callback:(Callback) callback;

@end
