//
//  NSURLRequest+Extra.h
//  Cubux
//
//  Created by spens on 10/06/16.
//  Copyright © 2016 Bern. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURLRequest (Extra)

- (void)logRequest;

@end
