//
//  Advert.h
//  TestApp
//
//  Created by Александр Сенченков on 10.03.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Advert : NSObject
@property (nonatomic, strong) NSString *advertID;
@property (nonatomic, strong) NSString *advertTitle;
@property (nonatomic, strong) NSString *advertDescription;
@property (nonatomic, strong) NSString *advertThumbnail;
@property (nonatomic, strong) NSString *advertUrlImage;
@property (nonatomic, strong) NSString *advertCreateDate;

//full Information
@property (nonatomic, strong) NSString *advertSourseUrl;
@property (nonatomic, strong) NSString *advertLead;
@property (nonatomic, strong) NSString *advertText;

-(void)fillAdvertWithParam:(NSDictionary *)param;

-(void)fillFullInfoWithParam:(NSDictionary *)param;
@end
