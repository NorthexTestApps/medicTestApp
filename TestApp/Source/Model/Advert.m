//
//  Advert.m
//  TestApp
//
//  Created by Александр Сенченков on 10.03.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import "Advert.h"
#import "NSString_stripHtml.h"

@implementation Advert

-(instancetype)init
{
    self = [super init];
    
    if (self) {
        
        self.advertID           = @"";
        self.advertTitle        = @"";
        self.advertUrlImage     = @"";
        self.advertThumbnail    = @"";
        self.advertCreateDate   = @"";
        self.advertDescription  = @"";
        
        self.advertSourseUrl    = @"";
    }
    
    return self;
}

-(void)fillAdvertWithParam:(NSDictionary *)param
{
    if (param[@"id"] && ![param[@"id"] isKindOfClass:[NSNull class]]) {
        
        self.advertID = [NSString stringWithFormat:@"%@", param[@"id"]];
        
    }
    
    if (param[@"created_at"] && ![param[@"created_at"] isKindOfClass:[NSNull class]]) {
        
        self.advertCreateDate = [NSString stringWithFormat:@"%@", param[@"created_at"]];
        
    }
    
    if (param[@"title"] && ![param[@"title"] isKindOfClass:[NSNull class]]) {
        
        self.advertTitle = [NSString stringWithFormat:@"%@", param[@"title"]];
        
    }
    
    if (param[@"image"] && ![param[@"image"] isKindOfClass:[NSNull class]]) {
        
        self.advertUrlImage = [NSString stringWithFormat:@"%@", param[@"image"]];
        
    }
    
    if (param[@"thumbnail"] && ![param[@"thumbnail"] isKindOfClass:[NSNull class]]) {
        
        self.advertThumbnail = [NSString stringWithFormat:@"%@", param[@"thumbnail"]];
        
    }
    
    if (param[@"description"] && ![param[@"description"] isKindOfClass:[NSNull class]]) {
        
        self.advertDescription = [NSString stringWithFormat:@"%@", param[@"description"]];
        
    }
   
}

-(void)fillFullInfoWithParam:(NSDictionary *)param
{
    
    if (param[@"lead"] && ![param[@"lead"] isKindOfClass:[NSNull class]]) {
        
        self.advertLead = [[NSString stringWithFormat:@"%@", param[@"lead"]] stripHtml];
        
    }
    
    if (param[@"source"] && ![param[@"source"] isKindOfClass:[NSNull class]]) {
        
        self.advertSourseUrl = [NSString stringWithFormat:@"%@", param[@"source"]];
        
    }
    
    if (param[@"text"] && ![param[@"text"] isKindOfClass:[NSNull class]]) {
        
        self.advertText = [[NSString stringWithFormat:@"%@", param[@"text"]] stripHtml];
        
    }
}
@end
