//
//  DetailsViewController.m
//  TestApp
//
//  Created by Александр Сенченков on 10.03.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import "DetailsViewController.h"
#import "DetailTableViewCell.h"
#import "MainTableViewCell.h"
#import "NSString_stripHtml.h"

@interface DetailsViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *adverts;

@end

@implementation DetailsViewController
{
    double sizeFont;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    sizeFont = 1.0;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"MainTableViewCell" bundle:nil] forCellReuseIdentifier:@"MainTableViewCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"DetailTableViewCell" bundle:nil] forCellReuseIdentifier:@"DetailTableViewCell"];
    
    [self getInfoAdvert];
    
     [self settingsViewController];
}

-(void)viewDidAppear:(BOOL)animated
{
    if (![_isScrolling boolValue]) {
        
        [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
        [self.navigationController.navigationBar setShadowImage:[UIImage new]];
        self.navigationController.navigationBar.translucent = YES;
        self.navigationController.view.backgroundColor = [UIColor clearColor];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView  {
    
    CGFloat alpha = scrollView.contentOffset.y/self.view.frame.size.height*0.8 ;
    
    if (![_isScrolling boolValue] && alpha > 1) {
         _isScrolling = @YES;
        return;
    }
    if (alpha > 0.994718) {
        
         alpha = 0.994718;
    }
 
    UIImage *img = [DetailsViewController imageWithColor:[UIColor colorWithRed:0.0f green:174.0/255.0f blue:195.0f/255.0f alpha:alpha]];
    
    [self.navigationController.navigationBar setBackgroundImage:img forBarMetrics:UIBarMetricsDefault];

    [self.navigationController.navigationBar setShadowImage:img];
}


+ (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 1.0f);
    [color setFill];
    UIRectFill(rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}


-(void)settingsViewController
{
    self.tableView.contentInset = UIEdgeInsetsMake(-64.0f, 0.0f, 0.0f, 0.0f);
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    
    UIBarButtonItem *leftBar = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    
    [leftBar setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *rightBar = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"fontSize"] style:UIBarButtonItemStylePlain target:self action:@selector(sizeAction)];
    
    [rightBar setTintColor:[UIColor whiteColor]];
    
    self.navigationItem.leftBarButtonItem = leftBar;
    
    self.navigationItem.rightBarButtonItem = rightBar;
    
}

-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)sizeAction
{
    if (sizeFont == 1.0f) {
        
        sizeFont = 1.2f;
    }
    else if (sizeFont == 1.2f)
    {
        sizeFont = 1.4f;
    }
    else if (sizeFont == 1.4f)
    {
        sizeFont = 1.6f;
    }
    else
    {
        sizeFont = 1.0f;
    }
    
    [self.tableView reloadData];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   return self.adverts.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        
        return [self calculateHeightInfo:sizeFont];
    }
    else
    {
        return 100;
    }
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        
        DetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DetailTableViewCell"];
        [cell layoutIfNeeded];
        cell.sizeFont = sizeFont;
        cell.advert = [self.adverts objectAtIndex:indexPath.row];
        CGRect widthBottom = CGRectMake(cell.backgroundGradient.bounds.origin.x, cell.backgroundGradient.bounds.origin.y, [UIScreen mainScreen].bounds.size.width, cell.backgroundGradient.bounds.size.height);
        CAGradientLayer *gradientBottom = [CAGradientLayer layer];
        gradientBottom.frame = widthBottom;
        gradientBottom.colors = @[(id)[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.3].CGColor, (id)[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0].CGColor];
        [cell.backgroundGradient.layer insertSublayer:gradientBottom atIndex:0];
        return cell;
    }
    else
    {
        MainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MainTableViewCell"];
        cell.advert = [self.adverts objectAtIndex:indexPath.row];
        return cell;
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row !=0 ) {
                
        DetailsViewController *DetailsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailsViewController"];
        DetailsViewController.advert = [self.adverts objectAtIndex:indexPath.row];
        DetailsViewController.isScrolling = @NO;
        [self.navigationController pushViewController:DetailsViewController animated:YES];
    }
}

-(void)getInfoAdvert
{
    [self showHUD];
    
    __weak typeof(self) weakSelf = self;
    
    [[NXConnectionManager instance] getInfoAboutNewWithParam:@{@"id" : self.advert.advertID} callback:^(id response, ErrorObj *error) {
       
        [weakSelf hideHUD];
        
        if (response) {
            
            if (!self.adverts) {
                
                self.adverts = [NSMutableArray new];
            }
            
            [self.advert fillFullInfoWithParam:response[@"data"]];
            
            [self.adverts addObject:self.advert];
            
            NSArray *spotlight = response[@"spotlight"];
            
            if (spotlight.count > 0) {
                
                [spotlight enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                   
                    Advert *advert = [[Advert alloc] init];
                    [advert fillAdvertWithParam:obj];
                    [self.adverts addObject:advert];
                }];
                
                [self.tableView reloadData];
            }

        }
    }];
}

-(CGFloat)calculateHeightInfo:(double)sizeKoef
{
    CGFloat result = 0;
    
    if (![self.advert.advertUrlImage isEqualToString:@""]) {
  
        result += 260.0f;
    }
    
    result +=140.5f;
   
    if (![self.advert.advertLead isEqualToString:@""]) {
        
        UILabel *leadLabel = [[UILabel alloc] init];
        
        NSDictionary *dickAttrMain = @{
                                       NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Medium" size:17.5 *sizeKoef],
                                       };
        NSAttributedString* attr = [[NSAttributedString alloc]  initWithString:self.advert.advertLead attributes:dickAttrMain];
        
        [leadLabel setAttributedText:attr];
        
        CGSize size = [leadLabel sizeThatFits:CGSizeMake(268, FLT_MAX)];
        
        result +=size.height;
    }
    
    if (![self.advert.advertText isEqualToString:@""]) {
        
        UITextView *calculationView = [[UITextView alloc] init];
        
        NSDictionary *dickAttrMain = @{
                                       NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:14.0*sizeKoef],
                                       };
        NSAttributedString* attr = [[NSAttributedString alloc]  initWithString:self.advert.advertText attributes:dickAttrMain];
        
        [calculationView setAttributedText:attr];
        
        CGSize size = [calculationView sizeThatFits:CGSizeMake(268, FLT_MAX)];
        
        result +=size.height;
    }
    else
        
    {
        result += 10.0f;
    }
    
    return result;
}
@end
