//
//  DetailTableViewCell.h
//  TestApp
//
//  Created by Александр Сенченков on 10.03.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Advert.h"
@interface DetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *createDate;
@property (weak, nonatomic) IBOutlet UIView *backgroundGradient;
@property (weak, nonatomic) IBOutlet UITextView *textAdvert;
@property (weak, nonatomic) IBOutlet UIButton *buttonSourse;
@property (nonatomic, strong) Advert *advert;
@property (nonatomic) double sizeFont;
@property (weak, nonatomic) IBOutlet UILabel *photoInfoLabel;

- (IBAction)buttonActionSourse:(id)sender;

@end
