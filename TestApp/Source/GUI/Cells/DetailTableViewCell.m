//
//  DetailTableViewCell.m
//  TestApp
//
//  Created by Александр Сенченков on 10.03.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import "DetailTableViewCell.h"
#import <UIImageView+AFNetworking.h>

@implementation DetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)buttonActionSourse:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.advert.advertSourseUrl] options:@{} completionHandler:nil];
}

-(NSDate *) toLocalTime:(NSString *)dateString
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    dateFormat.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"]; //iso 8601 format
    NSDate *output = [dateFormat dateFromString:dateString];
    return output;
}

-(void)setAdvert:(Advert *)advert
{
    _advert = advert;
    
    [self.textAdvert setFont:[UIFont fontWithName:@"HelveticaNeue" size:14*self.sizeFont]];
    [self.textAdvert setText:advert.advertText];
    
    self.titleLabel.text = advert.advertLead;
    [self.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:17.5*self.sizeFont]];
    
    NSArray *myArray = [advert.advertCreateDate componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"."]];
    
    NSDate *createDate = [self toLocalTime:[myArray objectAtIndex:0]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMM yyyy, HH:mm"];
    self.createDate.text = [dateFormat stringFromDate:createDate];
    [self.createDate setFont:[UIFont fontWithName:@"HelveticaNeue" size:12.5*self.sizeFont]];
    
    [self.photoInfoLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:12.5*self.sizeFont]];
    [self.buttonSourse.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:12.5*self.sizeFont]];
    
    __weak UIImageView *blockImage = self.headImage;
    
    [blockImage setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:advert.advertUrlImage]]
                      placeholderImage:nil
                               success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
                                   blockImage.alpha = 0.0;
                                   blockImage.image = image;
                                   [UIView animateWithDuration:0.0
                                                    animations:^{
                                                        blockImage.alpha = 1.0;
                                                    }];
                                   
                               }
                               failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
                                   
                               }];
}
@end
