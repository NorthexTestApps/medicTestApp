//
//  MainTableViewCell.m
//  TestApp
//
//  Created by Александр Сенченков on 10.03.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import "MainTableViewCell.h"
#import <UIImageView+AFNetworking.h>

@implementation MainTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(NSDate *) toLocalTime:(NSString *)dateString
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    dateFormat.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"]; //iso 8601 format
    NSDate *output = [dateFormat dateFromString:dateString];
    return output;
}


-(void)setAdvert:(Advert *)advert
{
    _advert = advert;
    
    NSArray *myArray = [advert.advertCreateDate componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"."]];
    
    NSDate *createDate = [self toLocalTime:[myArray objectAtIndex:0]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMM yyyy, HH:mm"];
    self.labelData.text = [dateFormat stringFromDate:createDate];
    self.labelText.text = [NSString stringWithFormat:@"%@ / %@",advert.advertTitle, advert.advertDescription];
    
    __weak UIImageView *blockImage = self.imagePhoto;
    
    [blockImage setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:advert.advertThumbnail]]
                      placeholderImage:nil
                               success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
                                   blockImage.alpha = 0.0;
                                   blockImage.image = image;
                                   [UIView animateWithDuration:0.3
                                                    animations:^{
                                                        blockImage.alpha = 1.0;
                                                    }];
                                   
                               }
                               failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
                                   
                               }];
}
@end
