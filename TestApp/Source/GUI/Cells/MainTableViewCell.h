//
//  MainTableViewCell.h
//  TestApp
//
//  Created by Александр Сенченков on 10.03.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Advert.h"

@interface MainTableViewCell : UITableViewCell

@property (nonatomic, strong) Advert *advert;
@property (weak, nonatomic) IBOutlet UILabel *labelText;
@property (weak, nonatomic) IBOutlet UILabel *labelData;
@property (weak, nonatomic) IBOutlet UIImageView *imagePhoto;

@end
