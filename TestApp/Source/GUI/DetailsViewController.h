//
//  DetailsViewController.h
//  TestApp
//
//  Created by Александр Сенченков on 10.03.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Advert.h"

@interface DetailsViewController : UIViewController
@property (nonatomic, strong) Advert *advert;
@property (nonatomic, strong) NSNumber *isScrolling;
@end
