//
//  MainViewController.m
//  TestApp
//
//  Created by Александр Сенченков on 10.03.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import "MainViewController.h"
#import "MainTableViewCell.h"
#import "DetailsViewController.h"

static const NSString *limit = @"20";

@interface MainViewController () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataTableArr;
@property (nonatomic, retain) NSNumber *listAdvert;
@property (nonatomic, assign) BOOL isEndlist;
@end

@implementation MainViewController
{
    UIView *tempFooter;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.listAdvert = @0;
    _isEndlist = NO;
    self.dataTableArr = [NSMutableArray new];
    [self getAdvertsWithPage:self.listAdvert isRefresh:NO];
    [self.tableView registerNib:[UINib nibWithNibName:@"MainTableViewCell" bundle:nil] forCellReuseIdentifier:@"MainTableViewCell"];
    
    
    tempFooter = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 50)];
    UIActivityIndicatorView *tableIndicxator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [tableIndicxator setColor:COLOR(0, 174, 195) ];
    tableIndicxator.hidesWhenStopped = YES;
    [tempFooter addSubview:tableIndicxator];
    tableIndicxator.center = CGPointMake(tempFooter.bounds.size.width / 2.0f, tempFooter.bounds.size.height / 2.0f);
    tableIndicxator.autoresizingMask = (UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin);
    [tableIndicxator startAnimating];
    //self.tableView.tableFooterView = tempFooter;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    UIImage *img = [MainViewController imageWithColor:[UIColor colorWithRed:0.0f green:174.0/255.0f blue:195.0f/255.0f alpha:0.994718]];
    
    [self.navigationController.navigationBar setBackgroundImage:img forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController.navigationBar setShadowImage:img];
    
}

+ (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0, 0, 1, 1);
    
    // create a 1 by 1 pixel context
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 1.0f);
    [color setFill];
    UIRectFill(rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataTableArr.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100.0f;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row > 13) {
        
        if (indexPath.row==self.dataTableArr.count-2 && _isEndlist == NO) {
            
            _listAdvert = [NSNumber numberWithInt:[_listAdvert intValue] + 1];
            self.tableView.tableFooterView = tempFooter;
            [self getAdvertsWithPage:_listAdvert isRefresh:NO];
            
        }
    }
    
    else
    {
        self.tableView.tableFooterView = nil;
    }
    
    MainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MainTableViewCell"];
    
    cell.advert = [self.dataTableArr objectAtIndex:indexPath.row];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    DetailsViewController *DetailsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailsViewController"];
    DetailsViewController.advert = [self.dataTableArr objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:DetailsViewController animated:YES];
}

#pragma mark Get info from server

-(void)getAdvertsWithPage:(NSNumber *)page isRefresh:(BOOL)isRefresh
{
    NSDictionary *param = @{@"page" : page,
                            @"limit" : limit
                            };
    
    if (isRefresh) {

        [self showHUD];
    }
    
    __weak typeof(self) weakSelf = self;
    
    [[NXConnectionManager instance] getAllNewsWithParam:param callback:^(id response, ErrorObj *error) {
       
        if (isRefresh) {
        
            [weakSelf hideHUD];
        }
 
        
        if (response) {
     
            if (!weakSelf.dataTableArr) {
                
                weakSelf.dataTableArr = [NSMutableArray new];
            }
            if (isRefresh)
            {
                [weakSelf.dataTableArr removeAllObjects];
            }
            
            NSArray *adverts = response[@"data"];
            
            if (adverts.count > 0) {
                
                [adverts enumerateObjectsUsingBlock:^(NSDictionary *objAdvert, NSUInteger idx, BOOL * _Nonnull stop) {
                    
                    Advert *advert = [[Advert alloc] init];
                    [advert fillAdvertWithParam:objAdvert];
                    [self.dataTableArr addObject:advert];
                    
                }];
                
                [self.tableView reloadData];
            }
            else
            {
                self.tableView.tableFooterView = nil;
                _isEndlist = YES;
                return ;
            }
            
        }
        else
        {
            self.tableView.tableFooterView = nil;
            _isEndlist = YES;
        }
        
        if (isRefresh) {
            
            [self.tableView setContentOffset:CGPointMake(0.0f, -self.tableView.contentInset.top) animated:YES];
            ;
            
        }
    }];
}

- (IBAction)refreshTable:(id)sender {
    
    _isEndlist = NO;
    _listAdvert = @0;
    [self getAdvertsWithPage:_listAdvert isRefresh:YES];
}

@end
