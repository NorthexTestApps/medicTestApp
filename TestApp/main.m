//
//  main.m
//  TestApp
//
//  Created by Александр Сенченков on 10.03.17.
//  Copyright © 2017 Александр Сенченков. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
